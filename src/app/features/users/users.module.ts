import { NgModule } from '@angular/core';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import { SharedModule } from "../../shared/shared.module";
import { UserService } from "../../core/services/user/user.service";

@NgModule({
  declarations: [
    UsersComponent,
  ],
  imports: [
    UsersRoutingModule,
    SharedModule,
  ],
  providers: [
    UserService,
  ]
})
export class UsersModule { }
