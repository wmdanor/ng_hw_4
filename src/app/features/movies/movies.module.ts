import { NgModule } from '@angular/core';
import { MoviesComponent } from './movies.component';
import { MoviesRoutingModule } from './movies-routing.module';
import { SharedModule } from "../../shared/shared.module";
import { MovieService } from "../../core/services/movie/movie.service";

@NgModule({
  declarations: [
    MoviesComponent,
  ],
  imports: [
    MoviesRoutingModule,
    SharedModule,
  ],
  providers: [
    MovieService
  ]
})
export class MoviesModule { }
